package com.lanlang.logindemo.entity;

public class LoginResponse {
    public int code;
    public String msg;
    public String token;

    public LoginResponse(int code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

    public LoginResponse(int code, String msg, String token)
    {
        this.code = code;
        this.msg = msg;
        this.token = token;
    }
}
