package com.lanlang.logindemo.interceptor;

import androidx.annotation.NonNull;

import com.lanlang.logindemo.utils.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CommonInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        // 获取当前时间戳
        String ts = String.valueOf(System.currentTimeMillis()/1000);
        String sign = Utils.encodeMD5(ts);
        Request request = chain.request().newBuilder().addHeader("ts", ts).addHeader("sign", sign).build();
        // 继续执行后面的逻辑
        Response response = chain.proceed(request);

        return response;
    }
}
