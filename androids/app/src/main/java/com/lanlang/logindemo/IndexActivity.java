package com.lanlang.logindemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.lanlang.logindemo.entity.Films;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class IndexActivity extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        // 获取页面布局
        listView = findViewById(R.id.mylist);

        // 请求后端数据并加载
        new Thread(){
            @Override
            public void run()
            {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://192.168.1.111:8084/films").get().build();
                Call call = client.newCall(request);
                try {
                    Response res = call.execute();
                    ResponseBody body = res.body();
                    String dataStr = body.string();
                    // 反序列化
                    Films obj = new Gson().fromJson(dataStr, Films.class);
                    if (obj.code == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyAdapter adapter = new MyAdapter(IndexActivity.this, obj);
                                listView.setAdapter(adapter);
                            }
                        });
                    } else {
                        showToast("首页加载失败");
                    }
                } catch (IOException e) {
                    Log.e("error", e.toString());
                }
            }
        }.start();
    }

    private void showToast(String msg)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(IndexActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}

class MyAdapter extends BaseAdapter {
    private Context context;
    private Films dataList;
    public MyAdapter(Context context, Films dataList)
    {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.films.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        //  根据布局文件实例化view
        TextView tv = (TextView)convertView.findViewById(R.id.name);
        tv.setText(this.dataList.films.get(position).name);

        TextView synopsis = (TextView)convertView.findViewById(R.id.synopsis);
        synopsis.setText(this.dataList.films.get(position).synopsis);


        ImageView img = (ImageView)convertView.findViewById(R.id.pic);
        //使用Glide加载图片  implementation 'com.github.bumptech.glide:glide:4.11.0'
        Glide.with(context)
                //加载地址
                .load(this.dataList.films.get(position).poster)
                //加载失败时，设置默认的图片
                .placeholder(R.mipmap.ic_launcher)
                //显示的位置
                .into(img);
        return convertView;
    }
}




