# java学习笔记

## 概念:
1. jdk: 开发环境(包含jre+jvm)
2. jre: 运行环境(包含jvm)
3. jvm: 虚拟机,执行器



## 编译过程:
> 源文件 -javac-> 字节码文件[class,jar,war] -jvm-> 虚拟机上运行



## 面向对象
// import java.lang.Object;
// java 中，所有类，都继承自 Object类--》java分类型--》不同类型不能相互赋值
// 但是，由于所有类都继承了Object类--》所有所有类的对象，都可以称为Object类的对象---》所有类型的变量，都可以赋值给Object类的对象

// 1.一个文件中只能存在一个public修饰的类
// 2.被public修饰的类为入口可访问的类
// 3.入口方法为public static  指定返回值:void
// 4.被public修饰的类名称必须与文件名称保持一致,否则会报错
// 5.import导入的包,末尾一定要带上分号

// 1. clone()
保护方法，实现对象的浅复制，只有实现了Cloneable接口才可以调用该方法，否则抛出CloneNotSupportedException异常。

// 2. getClass()
final方法，返回Class类型的对象，反射来获取对象。

//3. toString()
该方法用得比较多，一般子类都有覆盖，来获取对象的信息。

//4. finalize()
该方法用于释放资源。因为无法确定该方法什么时候被调用，很少使用。

//5. equals()
比较对象的内容是否相等

// 6. hashCode()
该方法用于哈希查找，重写了equals方法一般都要重写hashCode方法。这个方法在一些具有哈希功能的Collection中用到。

// 7. wait()
wait方法就是使当前线程等待该对象的锁，当前线程必须是该对象的拥有者，也就是具有该对象的锁。wait()方法一直等待，直到获得锁或者被中断。wait(long timeout)设定一个超时间隔，如果在规定时间内没有获得锁就返回。
调用该方法后当前线程进入睡眠状态，直到以下事件发生。
其他线程调用了该对象的notify方法。
其他线程调用了该对象的notifyAll方法。
其他线程调用了interrupt中断该线程。
时间间隔到了。
此时该线程就可以被调度了，如果是被中断的话就抛出一个InterruptedException异常。

// 8. notify()
该方法唤醒在该对象上等待的某个线程。

//9. notifyAll()
该方法唤醒在该对象上等待的所有线程


// 总结：
后期如果打印一个对象，我们想定制我们的打印格式，可以重写toString 方法---》所有类都会有这个方法--》所有类继承了Object类

### 对象与类
1) 类
// []内可以省略，|表示或单两个关键字不能同时出现
[public] [abstract|final] class 类名class_name [extends 继承的类名] [implements 实现的接口名] {
// 定义成员属性
属性类型1 属性名1;  // String name;
属性类型2 属性名2;  // int age;
// 定义静态属性（类属性）

    // 定义成员方法
    public int add(int a,int b){
        return a+b;
    }
  	// 定义静态方法（类方法）
  public static void speak(){
      System.out.println("说话");
  }

}

// 解释
public ：表示 共有 的意思。如果使用 public 修饰，则可以被其他类和程序访问。每个 Java 程序的主类都必须是 public 类，作为公共工具供其他类和程序使用的类应定义为 public 类
abstract ：类被 abstract 修饰，则该类为抽象类，抽象类不能被实例化，但抽象类中可以有抽象方法（使用 abstract 修饰的方法）和具体方法（没有使用 abstract 修饰的方法）。继承该抽象类的所有子类都必须实现该抽象类中的所有抽象方法（除非子类也是抽象类）
final ：如果类被 final 修饰，则不允许被继承
class ：声明类的关键字
class_name ：类的名称
extends ：表示继承其他类
implements ：表示实现某些接口

2) 类的属性
   // 语法
   [public|protected|private] [static][final] <变量类型> <变量名>
   // 解释
   public protected private ：用于表示成员变量的访问权限
   static ：表示该成员变量为类变量，也称为静态变量
   final ：表示将该成员变量声明为常量，其值无法更改
   变量类型 ：表示变量的类型
   变量名：表示变量名称

// 例如
public class Person {
  public String name;    // 姓名
  final int sex = 0;    // 性别：0表示女孩，1表示男孩
  private int age;    // 年龄
}


// this在类内部使用，代指当前实例[对象]，等同于python的self

3) 构造函数
   a) 构造方法与类同名
   b) 没有返回值，不需要void关键字
   c) 可以有多个，表示多种构造方式

4) 访问控制修饰符

| 访问范围         | private  | friendly(默认) | protected | public |
| ---------------- | -------- | -------------- | --------- | ------ |
| 同一个类         | 可访问   | 可访问         | 可访问    | 可访问 |
| 同一包中的其他类 | 不可访问 | 可访问         | 可访问    | 可访问 |
| 不同包中的子类   | 不可访问 | 不可访问       | 可访问    | 可访问 |
| 不同包中的非子类 | 不可访问 | 不可访问       | 不可访问  | 可访问 |

```java
//1  private
用 private 修饰的类成员，只能被该类自身的方法访问和修改，而不能被任何其他类（包括该类的子类）访问和引用。因此，private 修饰符具有最高的保护级别
  
//2 friendly（默认）
如果一个类没有访问控制符，说明它具有默认的访问控制特性。这种默认的访问控制权规定，该类只能被同一个包中的类访问和引用，而不能被其他包中的类使用，即使其他包中有该类的子类。这种访问特性又称为包访问性（package private）

//3  protected
用保护访问控制符 protected 修饰的类成员可以被三种类所访问：该类自身、与它在同一个包中的其他类以及在其他包中的该类的子类。使用 protected 修饰符的主要作用，是允许其他包中它的子类来访问父类的特定属性和方法，否则可以使用默认访问控制符。
  
//4 public
当一个类被声明为 public 时，它就具有了被其他包中的类访问的可能性，只要包中的其他类在程序中使用 import 语句引入 public 类，就可以访问和引用这个类
```

5) 静态方法与静态变量

// 在类中，使用 static 修饰符修饰的属性（成员变量）称为静态变量，也可以称为类变量，常量称为静态常量，方法称为静态方法或类方法，它们统称为静态成员，归整个类所有。

// 静态成员不依赖于类的特定实例，被类的所有实例共享，就是说 static 修饰的方法或者变量不需要依赖于对象来进行访问，只要这个类被加载，Java 虚拟机就可以根据类名找到它们

//********************静态变量*************************
类的成员变量可以分为以下两种：
静态变量（或称为类变量），指被 static 修饰的成员变量
实例变量，指没有被 static 修饰的成员变量

// 静态变量与实例变量的区别如下：
1）静态变量
运行时，Java 虚拟机只为静态变量分配一次内存，在加载类的过程中完成静态变量的内存分配
在类的内部，可以在任何方法内直接访问静态变量
在其他类中，可以通过类名访问该类中的静态变量

2）实例变量
每创建一个实例，Java 虚拟机就会为实例变量分配一次内存
在类的内部，可以在非静态方法中直接访问实例变量
在本类的静态方法或其他类中则需要通过类的实例对象进行访问

//********************静态方法********************
类的成员方法也可以分为以下两种：
静态方法（或称为类方法），指被 static 修饰的成员方法
实例方法，指没有被 static 修饰的成员方法


// 静态方法与实例方法的区别如下：
静态方法不需要通过它所属的类的任何实例就可以被调用，因此在静态方法中不能使用 this 关键字，也不能直接访问所属类的实例变量和实例方法，但是可以直接访问所属类的静态变量和静态方法。另外和 this 关键字一样, super 关键字也与类的特定实例相关，所以在静态方法中也不能使用 super 关键字
在实例方法中可以直接访问所属类的静态变量、静态方法、实例变量和实例方法


6) 类的继承

// 在 Java 中通过 extends 关键字可以申明一个类是从另外一个类继承而来的，一般形式如下：
// Java中的继承，只支持单继承，不支持多继承，但支持实现多个接口

class 父类 {
}

class 子类 extends 父类 {
}
class 子子类 extends 子类 {
}

  a) // 1 子类如果没写构造方法，默认使用父类无参构造，本案例名字为：固定名字
  // 2 子类如果没写构造方法,不会自动使用父类的有参构造，所以Person p=new Person("justin");用法是报错的

  b) super关键字
```java
class Animal{
      String name;
      public void Speak(){
      System.out.println("动物说话");
        }
    }
    class Person extends Animal {
    int age;
    public void Run(){
    System.out.println("人走路");
    }
        @Override
        public void Speak(){
            super.Speak(); // 代指父类对象，等同于python的super()
            System.out.println("人说话");
        }
    }
```

7) 重写(Override)与重载(Overload)
// 1 重写(Override)是子类对父类的允许访问的方法的实现过程进行重新编写, 返回值和形参都不能改变。即外壳不变，核心重写
// 2 重载(overloading) 是在一个类里面，方法名字相同，而参数不同。返回类型可以相同也可以不同
被重载的方法必须改变参数列表(参数个数或类型不一样)

```java
class Animal{
    String name;
    public Animal(){
        this.name="固定名字";
    }
    public Animal(String  name){
        this.name=name;
    }

    public void Speak(){
        System.out.println("动物说话");

    }
}
class Person extends Animal {
    int age;
    public void Run(){
        System.out.println("人走路");
    }
    // 重载方法
    public void Run(String ss){
        System.out.println(ss);
        System.out.println("走路");
    }
    // 重写方法Speak
    @Override
    public void Speak(){
        super.Speak(); // 代指父类对象，等同于python的super()
        System.out.println("人说话");
    }

    // 重载方法Speak
    public void Speak(String aa){
        super.Speak(); // 代指父类对象，等同于python的super()
        System.out.println("人说话");

    }
}
```



### 接口
1 接口（Interface），是一个抽象类型，是抽象方法的集合，接口通常以interface来声明。一个类通过实现接口的方法，从而来实现接口的抽象方法。

2 接口并不是类，编写接口的方式和类很相似，但是它们属于不同的概念。类描述对象的属性和方法。接口则包含类要实现的方法。

3 接口无法被实例化，但是可以被实现
一个实现接口的类，必须实现接口内所描述的所有方法，否则就必须声明为抽象类。接口类型可用来声明一个变量，他们可以成为一个空指针，或是被绑定在一个以此接口实现的对象。

4 接口与类相似点：
一个接口可以有多个方法
接口文件保存在 .java 结尾的文件中，文件名使用接口名

5 接口与类的区别：
接口不能用于实例化对象
接口没有构造方法
接口中所有的方法必须是抽象方法
接口不能包含成员变量，除了 static 和 final 变量
接口不是被类继承了，而是要被类实现
接口支持多继承

1) 声明
```java
[可见度] interface 接口名称 [extends 其他的接口名] {
        // 声明变量
        // 抽象方法
}

interface Duck {
    //任何类型 final, static 字段
    final String name="justin";
    public static int age = 19;

    //抽象方法
    public void Speak();
    //public void Run(){}; // 不能有具体实现
    public void Run(); // 不能有具体实现
}
```

2) 接口继承
```java
interface Duck {
    public void Speak();
    public void Run();
}

interface TDuck extends Duck{
    public void Flay();
}

// 接口的多继承:在Java中，类的多继承是不合法，但接口允许多继承。在接口的多继承中extends关键字只需要使用一次，在其后跟着继承接口
interface Foo extends Duck, TDuck{
}
```

3) 接口实现
```java
// 实现接口，必须实现接口中所有的方法
interface Duck {
    //抽象方法
    public void Speak();
    public void Run(); // 不能有具体实现
}

interface TDuck extends Duck{
    public void Flay();
}
class RealDuck implements TDuck{

    @Override
    public void Speak() {
        
    }

    @Override
    public void Run() {

    }

    @Override
    public void Flay() {

    }
}
```


注意点:
- java中一般不直接通过对象调用属性，而是通过方法来设置和获取属性【更安全】
- 




## 命令规范
### 文件名
1. 文件名称推荐使用[大]驼峰命令方式
2. 内部的public方法必须和文件名称保持一致
3. 类的内部只能允许存在一个public修饰的类


### 类和成员
1. 类名使用大驼峰命名
2. 命名的修饰符只能是public,或default(没有添加public,默认是这种方式)
3. 成员/方法的修饰符包含: public,protect,private,类的修饰只能是public
4. 属性和方法都可以使用static来修饰,有static修饰的就是所属类的,否则就是所属对象的
5. void代表返回值, 什么都没有返回就是void,例如:只是在里面进行打印操作


### 注释
1.注释使用跟php的一致,单行注释,多行注释,文档注释

### 语法
1.必须使用分号结尾

### 循环
1.if-else语法跟js的是一样的,都需要使用小括号和大括号进行包裹
2.存在while和do-while循环方式,使用方式跟js中的保持一致
3.for循环


## 数据类型
Java 语言支持 8 种基本数据类型：byte，short，int，long，float，double，char 和 boolean
![数据范围](./imgs/java_basic_types.png)

### 变量
1.类型必须定义在变量名称的前面
### 常量
1.必须会使用final进行修饰

### 字节类型
byte
【1字节】表示范围：-128 ~ 127 即：`-2^7 ~ 2^7 -1

### 字节有无符号
Python字节无符号（不含负数）： 0 ~ 255
0 1 2 3 4 5 ... 127  128  129  130 ... 255

Java字节是有符号（含有负数）：-128 ~ 127
0 1 2  3 4 5 ...127  -128 -127 - 126 -125 .... -2  -1



### 字符
char

### 字符串
String


1) 使用"+"进行拼接
2) 



### 整型
byte	带符号字节型	  8位	-128 ~ 127
short	带符号字节型	  16位	-32768 ~ 32767
int	带符号字节型	  32位	-231	-2147483648 ~ 2147483647
long	带符号字节型	  64位	-263	-9223372036854775808 ~ 9223372036854775807


### 浮点数
float
double

### 布尔值
bool

### 数组
int[]
String[]

### 容器类型 List
- ArrayList，连续的内存地址的存储（底层基于数组，内部自动扩容）
  1 ArrayList-->放多个元素--》只能放同一种类型
    ArrayList arrayList = new ArrayList();  // 用的少，因为没有指定类型，没指定就是Object类型，一般都会指定类型

- LinkedList，底层基于链表实现（底层基于链表）
  1 LinkedList-->放多个元素--》只能放同一种类型
    LinkedList常用方法:包含ArrayList的所有操作


### 容器类型 Set
Set是一个**接口**，常见实现这个接口的有两个类，用于实现不重复的多元素集合。

- HashSet，去重，无序。
能够进行运算:
  - 并集: h3.addAll(h2);     // 把h2的所有数据增加到h3中
  - 交集: h3.retainAll(h1);
  - 差集: h2.removeAll(h1);

- TreeSet，去重，内部默认排序（ascii、unicode）【不同的数据类型，无法进行比较】。

### 容器类型 Map
Map是一个接口，常见实现这个接口的有两个类，用于存储键值对。 ==> python中的字典

- HashMap，无序。
- TreeMap，默认根据key排序。（常用）



### 枚举类型 
Java 枚举是一个特殊的类，一般表示一组常量，比如一年的 4 个季节，一年的 12 个月份，一个星期的 7 天，方向有东南西北等。
Java 枚举类使用 enum 关键字来定义，各个常量使用逗号 , 来分割。
他是一种特殊的类,里面可以定义对应的成员方法

```java
// 类外部
enum Color 
{ 
    RED, GREEN, BLUE; 
} 
  
public class Test 
{ 
    // 执行输出结果
    public static void main(String[] args) 
    { 
        Color c1 = Color.RED; 
        System.out.println(c1); 
    } 
}


// 类外部
public class Test
{
    enum Color
    {
        RED, GREEN, BLUE;
    }

    // 执行输出结果
    public static void main(String[] args)
    {
        Color c1 = Color.RED;
        System.out.println(c1);
    }
}


enum Week {
    SUNDAY(0, "星期日"),
    MONDAY(1, "星期一"),
    TUESDAY(2, "星期二"),
    WEDNESDAY(3, "星期三"),
    THURSDAY(4, "星期四"),
    FRIDAY(5, "星期五"),
    SATURDAY(6, "星期六");
    public int id;
    public String meaning;

    Week(int id, String meaning) {
        this.id = id;
        this.meaning = meaning;
    }
}

```


### 包
// 为了更好地组织类，Java 提供了包机制，用于区别类名的命名空间。

// 包的作用
1、把功能相似或相关的类或接口组织在同一个包中，方便类的查找和使用

2、同文件夹一样，包采用了树形目录的存储方式。同一个包中的类名字是不同的，不同的包中的类的名字是可以相同的，当同时调用两个不同包中相同类名的类时，应该加上包名加以区别。因此，包可以避免名字冲突。

3、包也限定了访问权限，拥有包访问权限的类才能访问某个包中的类。










