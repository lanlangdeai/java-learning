import com.utils.Helper;

import java.util.*;

class Person
{
    String name;  // 用到的属性, 需要定义

    public Person()
    {

    }
    //构造方法  必须public 没有返回值，必须跟类同名
    public Person(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }
}

enum Sex {
    MALE, FEMALE
}


public class Main {
    public static void main(String[] args) {
//        add();
//        input();
//        str2Byte();
//        strs();
//        StrChar();
//        objects();
//        lists();
//        sets();
//        maps();

//        enums();
        packages();
    }

    public static void packages()
    {
        Helper helper = new Helper();
        Integer res = helper.Add(1, 2);
        System.out.println("Add result: " + res);
    }


    public static void enums()
    {
        Sex male = Sex.MALE;
        System.out.println(male);
    }

    public static void objects()
    {
        Person p1 = new Person();
        p1.name = "xing";
        System.out.println("类型");
        System.out.println(p1.getClass());
        if (p1 instanceof Person) {
            System.out.println("所属Person类");
        }
        System.out.println(p1.toString());
        System.out.println(p1.getName());
    }

    public static void maps()
    {
        HashMap<String,String> user = new HashMap<>();
        user.put("name", "xing");
        user.put("age", "18");
        System.out.println(user);

        user.remove("age");
        System.out.println(user);
        System.out.println(user.size());
        // 包含key的, 包含value的
        boolean existsKey = user.containsKey("age");
        boolean existsValue = user.containsValue("xing");
        System.out.println(existsKey);
        System.out.println(existsValue);
        // 替换
        user.replace("name", "兴");
        System.out.println(user);

        // 遍历
        // 方式一
        for (String kk : user.keySet()) {
            System.out.println(user.get(kk));
        }
        // 方式二
        for (Map.Entry<String, String> item: user.entrySet()) {
            System.out.println(item);
            System.out.println(item.getKey());
            System.out.println(item.getValue());
        }
        // 方式三
        Set s = user.entrySet();
        Iterator it = s.iterator();
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry)it.next();
            String k = (String) me.getKey();
            String v = (String) me.getValue();
            System.out.println(k);
            System.out.println(v);
        }


        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        // TreeMap：按key排序
        HashMap<String, Integer> person = new HashMap<>();
        person.put("first", 1);
        person.put("last", 2);
        System.out.println(person);

    }

    public static void sets()
    {
        HashSet<String> names = new HashSet<>();
        names.add("xing");
        names.add("ai");
        names.add("xia");
        System.out.println(names);
        System.out.println(names.size());
        System.out.printf("是否存在:%b\n", names.contains("爱"));

        TreeSet<Integer> ages = new TreeSet<>();
        ages.add(18);
        ages.add(1);
        ages.add(3);

    }

    public static void lists()
    {
        ArrayList<String> nameLists = new ArrayList<String>();
        nameLists.add("xing");  // 添加值
        nameLists.add("ai");
        nameLists.add("xia");
        System.out.println(nameLists);
        System.out.println("第2个:"+nameLists.get(1));  // 取值
        // 修改某个值
        nameLists.set(1, "爱");
        System.out.println("第2个:"+nameLists.get(1));
        // 删除
        nameLists.remove(2);
        System.out.println(nameLists);
        // 数组容量
        System.out.println("size:"+nameLists.size());
        // 是否包含
        System.out.println(nameLists.contains("兴"));


        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        // 迭代数组
        for (String name : nameLists) {
            System.out.println(name);
        }



        LinkedList<Integer> ages = new LinkedList<Integer>();
        ages.add(20);
        ages.add(30);
        ages.add(40);
        System.out.println(ages);

        ages.push(111);  // 放在首位(左侧推入)
        ages.addFirst(11);  // 放在第一位(左侧第一个)
        ages.addLast(222); // 最后一个
        System.out.println("新的age:");
        System.out.println(ages);
        // 进行迭代(使用迭代器)
        Iterator items = ages.iterator();
        while (items.hasNext()) {
            System.out.println(items.next());
        }
    }



    public static void StrChar()
    {
        char[] names = new char[]{'兴', '爱', '霞'};
        // 转成字符串
        String nameStr = new String(names);
        System.out.println("名称:" + nameStr);

        // 转字符数组
        char[] new_names = nameStr.toCharArray();
        System.out.println("长度:" + new_names.length);
        System.out.println("第一个字符:" + new_names[0]);
    }


    // 字符串相关操作
    public static void strs() {
        String name = "lanlang = X-wolf ";
        System.out.println("名称:"+name);
        // 字符串长度
        System.out.println("字符串长度:"+name.length());

        // 去除空白字符
        String new_name = name.trim();
        System.out.println("新名称:"+new_name);
        System.out.printf("长度:%d\n", new_name.length());
        // 是否包含
        String x = "X";
        // 是否以*开始
        boolean isContain = new_name.contains(x);
        System.out.printf("是否包含字符串:%s, %b\n", x, isContain);

        // 大小写转换
        String new_name1 = new_name.toLowerCase();
        String new_name2 = new_name.toUpperCase();
        System.out.println("小写:"+new_name1);
        System.out.println("大写:"+new_name2);

        // 是否相等 equals

        // 默认追加
        String new_name3 = new_name.concat("1130");  // 串联/追加
        System.out.println("追加字符串后:"+new_name3);


        StringBuilder languages = new StringBuilder();
        languages.append("Php");
        languages.append("\n");
        languages.append("Python");
        languages.append("\n");
        languages.append("Golang");
        languages.append("\n");
        languages.append("Java");
        languages.append("\n");
        languages.append("C");
        System.out.println("languages:"+languages.toString());
    }

//    字符串转字节数组
    public static void str2Byte()
    {
        String name = "蓝狼";
        byte[] nameBytes = name.getBytes();
//        System.out.println(nameBytes);  // [B@1c4af82c
        System.out.println("转成字节数组");
        System.out.println(Arrays.toString(nameBytes));  // [-24, -109, -99, -25, -117, -68]


    }

    public static void input()
    {
        // 输入 scanner
        System.out.println("Please enter your name");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        System.out.println("Your name is " + name);
    }


    public static void add() {
        System.out.println("Hello world!");
        int num1 = 1, num2 = 2;  // 同时声明多个变量
        int total = Test.add(num1, num2);
        System.out.println("总计为:" + total);  // 直接进行拼接, 不需要进行转换
    }
}

// 1.一个文件中只能存在一个public修饰的类
// 2.被public修饰的类为入口可访问的类
// 3.入口方法为public static  指定返回值:void
// 4.被public修饰的类名称必须与文件名称保持一致,否则会报错
// 5.import导入的包,末尾一定要带上分号


class Test {
    public static int add(int num1, int num2) {
        return num1 + num2;
    }
}