import java.util.Arrays;

public class Base {

    public static void main(String[] args) {
        // 基础数据类型
        // 1.字节 byte
        byte b = 90;
        // 2.字符 char, 只允许单个
        char c = 'a';
        // 3.字符串
        String s = new String("hello world");
        String s2 = "hello world";
        // 4. 整型
        short ss = 11;
        int i = 2;
        long l = 222222222;
        // 5.浮点
        float f = 1.0f;
        double d = 1.00000001d;

        // 6.布尔值
        boolean isTrue = true, isFalse = false;
        // 7. 数组
        String[] a1 = new String[3]; // 声明,不需要传入具体指
        String[] sss = {"xing", "li"}; // 无需指定类型
        System.out.println(s);
        // 7.1字节数组
        byte[] bb = new byte[3];  // 仅声明数组中的数量, 并未进行赋值
        bb[0] = 91;
        System.out.println(bb);
        System.out.println(Arrays.toString(bb));
        // 7.2字符数组
        char[] cc = new char[]{'A', '好'};  // 直接赋值,无需声明数组中元素的个数
        System.out.println(Arrays.toString(cc));
        // 处理方式




//        loop();
//        fors();
//        System.out.println("hello, world");
//        // 这是单行注释
//        /*
//        这是多行注释
//        */
//        String name = "xing";
//        final int age = 18;
//        System.out.printf("我的名称:%s, 今年:%d\n", name, age);

//        System.out.println("请输入您的分数:");
//////        接收输入
//        Scanner scanner = new Scanner(System.in);
////        String content = scanner.nextLine();
//        String content = scanner.nextLine();
////        System.out.println("输入的内容:" + content);
//        // 转成
//        int score = Integer.parseInt(content);
//        if (score > 90) {
//            System.out.println("优秀");
//        } else if (score > 80) {
//            System.out.println("良好");
//        } else if (score > 60) {
//            System.out.println("不错");
//        } else {
//            System.out.println("差劲");
//        }
    }

    public static void loop() {
//        循环
        int i = 0;
        while (i < 10) {
            System.out.println("当前值:"+i);  // 转换成字符串, 使用+会自动进行转换
//            Integer.toString(i);
            i++;
        }

//        do {
//
//        } while ();


    }

    public static void fors() {
        for (int i = 0; i < 10; i++) {
            System.out.println("当前数值:"+i);
        }

        // for in
        String[] names = {"xing", "li", "lixing"};
        for (String name:names) {  // 便利拿到的是他的值
            System.out.println("名称:"+name);
        }
    }

    public static void basic()
    {
        boolean is_ok = true;
        System.out.println("是否成功:"+is_ok);
        System.out.printf("是否可以: %b\n", is_ok);

        // 字符和字符串
        char name = 'X';
        String realName = "lixing";  // 字符串必须使用双引号引起来
        // 字符转字符串
        // 1
        String nameStr = String.valueOf(name);
        // 2
        String nameStr2 = "" + name;
        // 3
        String nameStr3 = Character.toString(name);
        System.out.println("字符转字符串1:" + nameStr);
        System.out.println("字符转字符串2:" + nameStr2);
        System.out.println("字符转字符串3:" + nameStr3);
        // 字符串转字符

        char[] chars = realName.toCharArray();
        System.out.println("字符串转字符1:" + Arrays.toString(chars));

    }
}
